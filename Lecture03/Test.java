import java.util.Scanner;

//Create a class called Addition
public class Test
{

    //Create main method for java application
    public static void main(String[] argv)
    {

        //Create a scanner
        Scanner input = new Scanner(System.in);
        
        int number1;  //var to hold number 1
        int number2;  //var to hold number 2
        int sum;      //var to hold sum
        
        //Prompt user number 1
        System.out.print("Enter number1:");
        //get number1 from user
        number1 = input.nextInt();
        
        //Prompt user for numebr 2
        System.out.print("Enter number2:");
        //get number 2 from user
        number2 = input.nextInt();
        
        //calculate sum and save it
        sum = number1 + number2;
        
        //Print result
        System.out.printf("The %d sum is: %d%n", 10, sum);
    }
}