public class BreakTest
{
    public static void main(String[] argv)
    {
        int count;
        for (count=0;count<=10;count+=1)
        {
            if (count == 5)
            {
                continue;
            }
            System.out.printf("%d ", count);
        }
        
        System.out.printf("The loop broke at: %d\n",count);
    }

}