import java.util.Scanner;

public class MaxFinder
{
    public static void main(String[] argv)
    {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Enter three numbers:");
        double number1 = input.nextInt();
        double number2 = input.nextInt();
        double number3 = input.nextInt();
        
        double result = maximum(number1, number2, number3);
        
        System.out.println("The max number is: " + result);
    }
    
    public static double maximum(double x, double y, double z)
    {
        double maximumValue = x;
        
        if (y > maximumValue)
        {
            maximumValue = y;
        }
        if (z > maximumValue)
        {
            maximumValue = z;
        }
        
        return maximumValue;
    }
}