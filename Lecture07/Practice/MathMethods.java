public class MathMethods
{
    public static void main(String[] argv)
    {
        double x = Math.pow(10,2);
        System.out.printf("%f%n",x);
        
        double y = Math.ceil(12.68);
        System.out.printf("%f%n",y);
        
        double z = Math.max(12,68);
        System.out.printf("%f%n",z);
        
        System.out.printf("%f%n",Math.PI);
        
    }
}