public enum Book
{
    JHTP("Java How to Program", "2015"),
    CHTP("C How to Program","2013"),
    CPPHTP("C++ How to program", "2011"),
    HPC("High perfomance computing");
    
    private final String title;
    private final String year;
    
    Book(String title, String year)
    {
        this.title = title;
        this.year = year;
    }
    
    Book(String title)
    {
        this(title, "2015");
    }
    
    public String getTitle()
    {
        return this.title;
    }
    
    public String getYear()
    {
        return this.year;
    }
}