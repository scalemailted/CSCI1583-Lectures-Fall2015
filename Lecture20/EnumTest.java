import java.util.EnumSet;

public class EnumTest
{
    public static void main(String[] args)
    {
        System.out.println("All books:");
        for (Book book : Book.values())
        {
            System.out.printf("%-10s%-45s\n",book.getTitle(), book.getYear());
        }
    }
}