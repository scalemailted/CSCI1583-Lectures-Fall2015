public class Tester
{
    public static void main(String[] args)
    {
        System.out.println(!(1 * 3 != 3));
        System.out.println((10.0 >= 11.0) || (5 % 6 == 0));
        System.out.println(245 * 2 + 14 + "eva!");
        System.out.println(-5 * (6 + 3) /4);
        System.out.println(45 / 4);
        
        System.out.println(numNickels(0.11));
        System.out.println(numNickels(3.43));
    }


    public static int numNickels(double dollars)
    {
	    return (int)(dollars / 0.05);
    }


}