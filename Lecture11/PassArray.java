public class PassArray
{
    public static void main(String[] args)
    {
        int[] array = {1,2,3,4,5};
        
        displayArray(array);
        modifyArray(array);
        displayArray(array);
        displayElement(array[3]);

    }
    
    public static void displayArray(int[] array2 )
    {
        for (int i : array2)
        {
            System.out.printf("%d\n",i);
        }
    }
    
    public static void modifyArray(int[] array2 )
    {
        for (int i=0; i< array2.length; i++)
        {
            array2[i] *=5;
        }
    }
    
    public static void displayElement(int element )
    {
        System.out.print(element);
    }
}