//Poll analysis program for ranking food
public class StudentPoll
{

    public static void main(String[] args)
    {

        //data for student responses
        int[] responses = {1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,13};

        //array to hold the frequencies
        int[] frequency = new int[6];

        //for each answer, select responses element ans use that value as 
        //frequency index to determine element to increment
        for (int answer=0; answer < responses.length;answer++)
        {
            try 
            {
                int thisRank  = responses[answer];
                frequency[thisRank]++; 
            } 
            catch(ArrayIndexOutOfBoundsException e) 
            {
                //System.out.println(e);
                //System.out.printf("\tresponses[%d]=%d\n\n",answer, responses[answer]);
            }
        }
        
        System.out.printf("%s%10s\n","Rating","Freq");
        for(int ranking=1;ranking<frequency.length;ranking++)
        {
            System.out.printf("%6d%10d\n",ranking,frequency[ranking]);
        }

    }

}