public class EnhancedFor
{
    public static void main(String[] args)
    {
        //int[] array = {87, 67, 23, 43, 58, 34};
        //int total = 0;
        String[] names= {"Tom", "Mary", "Sue", "John"};
 
        for(String name : names)
        {
            name = "Ted";
            System.out.printf("%s\n",name);
 
        }
        
        for(String name : names)
        {
            System.out.printf("%s\n",name);
 
        }

        
    
    }
}