public class Tester
{
    public static void main(String[] args)
    {
        CommissionEmployee e1 = new CommissionEmployee("tom","tomson","123-45-6789", 50000,0.15);
        System.out.println(e1);
        System.out.println(e1.earnings());
        
        BasePlusCommissionEmployee e2 = new BasePlusCommissionEmployee("kim","kimson","123-45-6789", 150000,0.25, 50.00);
        System.out.println(e2);
        System.out.println(e2.earnings());
    }
}