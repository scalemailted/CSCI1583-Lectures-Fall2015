public class BasePlusCommissionEmployee extends CommissionEmployee
{
    private double baseSalary;
    
    public BasePlusCommissionEmployee(String firstName, String lastName, String socialSecurity, double grossSale, double commissionRate, double baseSalary)
    {
        super(firstName, lastName, socialSecurity, grossSale, commissionRate);
        this.baseSalary = baseSalary;
    }
    
    public void setBaseSalary(double baseSalary)
    {
        this.baseSalary = baseSalary;
    }
    
    public double getBaseSalary()
    {
        return this.baseSalary;
    }
    
    public double earnings()
    {
        return super.earnings() + this.baseSalary;
    }
    
    public String toString()
    {
        return String.format("%s, %s, %s, sales: %f, rate: %f salaey: %f BasePlusCommissionEmployee", super.getLastName(), super.getFirstName(), super.getSocialSecurity(), super.getGrossSale(), super.getCommissionRate(), this.baseSalary);
    }
}