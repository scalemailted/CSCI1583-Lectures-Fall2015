public class Date
{
    //class properties
    private int month;
    private int day;
    private int year;
    
    //for validity to check day
    private static final int[] DAYS_OF_MONTH ={0, 31, 28, 31, 30, 31, 30 ,31, 31, 30, 31, 30, 31};
    
    //constructor
    public Date(int month, int day, int year)
    {
        //check if month is range
        if(month <= 0 || month > 12)
        {
            throw new IllegalArgumentException("month must be between 1 to 12");
        }
        
        //check if day in range
        if (day <=0 || day > DAYS_OF_MONTH[month])
        {
            throw new IllegalArgumentException("The given day cannot exist in that month");
        }
        
        this.month = month;
        this.day = day;
        this.year = year;
    }
    
    public String toString()
    {
        return String.format("%d/%d/%d",this.month,this.day,this.year);
    }
    
}