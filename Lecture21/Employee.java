public class Employee
{
    //class properties
    private static int count = 0; 
    
    //instance properties
    private String firstName;
    private String lastName;
    private int id;
    private Date hireDate;
    private Date birthDate;
    
    //constructor
    public Employee(String firstName, String lastName, int id, Date hireDate, Date birthDate)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.birthDate = birthDate;
        this.hireDate = hireDate;
        Employee.count++;
    }
    
    //toString
    public String toString()
    {
        return String.format("%s, %s id:%d Hired: %s, Birthday: %s",this.lastName, this.firstName, this.id, this.hireDate, this.birthDate);
    }
    
    public static int getCount()
    {
        return Employee.count;
    }
}