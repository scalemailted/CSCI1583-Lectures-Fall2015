public class CommissionEmployee extends Object
{
    //properties
    private final String firstName;
    private final String lastName;
    private final String socialSecurity;
    private double grossSale;
    private double commissionRate;
    
    //constructor
    public CommissionEmployee(String firstName, String lastName, String socialSecurity, double grossSale, double commissionRate)
    {
        if (grossSale < 0.0)
        {
            throw new IllegalArgumentException("Gross sales must be >= 0.0");
        }
        
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurity = socialSecurity;
        this.grossSale = grossSale;
        this.commissionRate = commissionRate;
    }
    
    public String getFirstName()
    {
        return this.firstName;
    }
    
    public String getLastName()
    {
        return this.lastName;
    }
    
    public String getSocialSecurity()
    {
        return this.socialSecurity;
    }
    
    public double getGrossSale()
    {
        return this.grossSale;
    }
    
    public double getCommissionRate()
    {
        return this.commissionRate;
    }
    
    public void setCommissionRate(double commissionRate)
    {
        this.commissionRate = commissionRate;
    }
    
    public void setGrossSales(double grossSale)
    {
        this.grossSale = grossSale;
    }
    
    public double earnings()
    {
        return this.commissionRate * this.grossSale;
    }
    
    public String toString()
    {
        return String.format("%s, %s, %s, sales: %f, rate: %f CommissionEmployee", this.lastName, this.firstName, this.socialSecurity, this.grossSale, this.commissionRate);
    }
}