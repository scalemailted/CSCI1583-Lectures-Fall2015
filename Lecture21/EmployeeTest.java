public class EmployeeTest
{
    public static void main(String[] args)
    {
        System.out.printf("%d employees\n", Employee.getCount());
        Date birth = new Date(7, 24, 1965);
        Date hire = new Date(1,31,1999);
        Employee e1 = new Employee("Bob", "Blue", 1, hire, birth);
        System.out.println(e1);
        
        System.out.printf("%d employees\n", Employee.getCount());
        Employee e2 = new Employee("Bob", "Blue", 1, hire, birth);
        System.out.printf("%d employees\n", Employee.getCount());
        
    }
}