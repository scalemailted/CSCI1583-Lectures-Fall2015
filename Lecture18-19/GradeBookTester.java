public class GradeBookTester
{
    public static void main(String[] args)
    {
        int[] grades = {100, 23, 21, 0, 78, 90, 69, 82, 42, 22, 75};
        GradeBook myGradeBook = new GradeBook("Linear Algebra", grades);
        //Print the course name
        System.out.printf("Gradebook for %s\n",myGradeBook.getCourseName());
        //Print the grades
        myGradeBook.processGrades();
    }
}