public class Time1
{
    //properties
    private int hour;
    private int minute;
    private int second;
    
    public Time1()
    {
        this(0, 0, 0);
    }
    
    public Time1(int hour)
    {
        this(hour, 0, 0);
    }
    
    public Time1(int hour, int minute)
    {
        this(hour, minute, 0);
    }
    
    public Time1(int hour, int minute, int second)
    {
        if (hour < 0 || hour >= 24)
        {
            throw new IllegalArgumentException("hour must be 0-23");
        }
        if (minute < 0 || minute >= 60)
        {
            throw new IllegalArgumentException("minute must be 0-59");
        }
        if (second < 0 || second >= 60)
        {
            throw new IllegalArgumentException("second must be 0-59");
        }
        
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }
    
    public void setTime(int hour, int minute, int second)
    {
        if (hour < 0 || hour >= 24 
            || minute < 0 || minute >= 60 
            || second < 0 || second >= 60)
        {
            throw new IllegalArgumentException("hour, minute, or second was out of range");
        }
            
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }
    
    public String toString()
    {
        int hour;
        if (this.hour == 0 || this.hour == 12)
        {
            hour = 12;
        }
        else
        {
            hour = this.hour%12;
        }
        
        String timeOfDay;
        if (this.hour < 12)
        {
            timeOfDay = "AM";
        }
        else
        {
            timeOfDay = "PM";
        }
        String time = String.format("%d:%02d:%02d%s", hour,this.minute,this.second, timeOfDay);
        return time;
    }
    
    public String toUniversal()
    {
        return String.format("%d:%02d:%02d", this.hour,this.minute,this.second);
    }
}