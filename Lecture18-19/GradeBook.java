public class GradeBook
{
    //Properties
    private String courseName;  //course name
    private int[] grades;       //collection of grades
    
    //Constructor
    public GradeBook(String courseName, int[] grades)
    {
        this.courseName = courseName;
        this.grades = grades;
    }
    
    //Set coursename
    public void setCourseName(String courseName)
    {
        this.courseName = courseName;
    }
    
    //Get the coursename
    public String getCourseName()
    {
        return this.courseName;
    }
    
    //process the grades
    public void processGrades()
    {
        //output grades in array
        outputGrades();
    
        
        //print the class average
        System.out.printf("\nClass average is %.2f\n", getAverage());
        
        //print the lowest grade & highest grade
        System.out.printf("\nThe lowest grade: %d\n", getMinimum());
        System.out.printf("\nThe highest grade: %d\n", getMaximum());
        
        //print a bar chart of the grades
        outputBarChart();
    }
    
    public void outputGrades()
    {
        System.out.printf("The grades are:\n");
        for(int grade : this.grades)
        {
            System.out.printf("%d\n",grade);
        }
    }
    
    public double getAverage()
    {
        double total = 0.0;
        for(int grade : this.grades)
        {
            total += grade;
        }
        double average = total/this.grades.length;
        return average;
    }
    
    public int getMinimum()
    {
        int lowGrade = this.grades[0];
        for (int grade: this.grades)
        {
            if (grade < lowGrade)
            {
                lowGrade = grade;
            }
        }
        return lowGrade;
    }
    
    public int getMaximum()
    {
        int highGrade = this.grades[0];
        for (int grade: this.grades)
        {
            if (grade > highGrade)
            {
                highGrade = grade;
            }
        }
        return highGrade;
    }
    
    public void outputBarChart()
    {
        System.out.println("Grade distribution:\n");
        //Stores frequency of grades
        int[] frequency = new int[11];
        
        //for each grade frequency, increment freq array
        for (int grade: this.grades)
        {
            int indexFreq = grade/10;
            frequency[indexFreq]++;
        }
        //print the frequency
        for (int count=0; count< frequency.length; count++)
        {
            if (count == 10)
            {
                System.out.printf("%5d: ", 100);
            }
            else
            {
                System.out.printf("%02d-%02d:", count*10, count*10+9);
            }
            for (int stars=0; stars< frequency[count]; stars++)
            {
                System.out.print("*");
            }
            System.out.print("\n");
        }
        
        
    }
    
    
}