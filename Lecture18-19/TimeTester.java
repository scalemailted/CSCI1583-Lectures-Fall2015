public class TimeTester
{
    public static void main(String[] args)
    {
        Time1 time = new Time1(23,0,30);
        System.out.println(time);
        System.out.println(time.toUniversal());
        time.setTime(13,27,43);
        System.out.println(time);
        System.out.println(time.toUniversal());
        try
        {
            time.setTime(99,99,99);   
        }
        catch (IllegalArgumentException e)
        {
            System.out.println(e.getMessage());
        }
        System.out.println(time);
        System.out.println(time.toUniversal());
    }
}