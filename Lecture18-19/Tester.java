public class Tester
{
    public static void main(String[] args)
    {
        Time1 time = new Time1();
        System.out.println(time);
        time.setTime(13,27,6);
        System.out.println(time);
        try
        {
            time.setTime(99,99,99);
        }
        catch (IllegalArgumentException e)
        {
            System.out.println(e.getMessage());
        }
        System.out.println(time);
    }
}