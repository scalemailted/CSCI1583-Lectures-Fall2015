import java.util.Random;

public class Craps
{
    private static final Random randomNumbers = new Random();
    
    private enum Status {CONTINUE, WON, LOST};
    
    private static final int SNAKE_EYES = 2;
    private static final int TREY = 3;
    private static final int SEVEN = 7;
    private static final int ELEVEN = 11;
    private static final int BOX_CARS = 12;
    
    public static void main(String[] argv)
    {
        int myPoints = 0;
        Status gameStatus;
        
        int sumOfDice = rollDice();
        
        if (sumOfDice == SNAKE_EYES || sumOfDice == ELEVEN)
        {
            gameStatus = Status.WON;
        }
        else if (sumOfDice == SNAKE_EYES || sumOfDice == TREY || sumOfDice == BOX_CARS)
        {
            gameStatus = Status.LOST;
        }
        else
        {
            gameStatus = Status.CONTINUE;
            myPoints = sumOfDice;
            System.out.printf("Point is %d%n", myPoints);
        }
    
        while (gameStatus == Status.CONTINUE)
        {
            sumOfDice = rollDice();
        
            if (sumOfDice == myPoints)
            {
                gameStatus = Status.WON;
            }
            else if (sumOfDice == SEVEN)
            {
                gameStatus = Status.LOST;
            }
        }
    
        if (gameStatus == Status.WON)
        {
            System.out.println("Player wins");
        }
        else
        {
            System.out.println("Player loses.");
        }
    }

    public static int rollDice()
    {
        int die1 = 1 + randomNumbers.nextInt(6);
        int die2 = 1 + randomNumbers.nextInt(6);
        int sum = die1 + die2;
        System.out.printf("Player rolled %d + %d = %d\n",die1,die2,sum);
        return sum;
    }
}