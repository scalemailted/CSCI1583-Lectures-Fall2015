import java.util.Random;

//Craps class simulates the dice game Craps
public class Craps2
{

    /*Variables */
    //Create random number generator
    private static Random randomGenerator = new Random();
    
    //manage whether we won, lost, continue
    private enum Status {CONTINUE, WON, LOST};
    
    //Create constants for dice states
    private static final int SNAKE_EYES = 2;
    private static final int TREY = 3;
    private static final int SEVEN = 7;
    private static final int ELEVEN = 11;
    private static final int BOXCAR = 12;
    
    
    //method plays one game of craps
    public static void main(String[] args)
    {
        //randomGenerator.setSeed(0);
        //variable to hold our point
        int myPoint = 0;
        
        //variable to control loop
        Status gameStatus;
        
        //variable to hold diceSum
        int diceSum;
        
        //get a dice roll
        diceSum = rollDice();
        
        //determine if you won, lost, or continue
        //if 7 or 11 you win
        if ((diceSum == SEVEN) || (diceSum == ELEVEN))
        {
            gameStatus =  Status.WON;
        }
        //if 2, 3, 12 you lose
        else if ((diceSum == SNAKE_EYES) || (diceSum == TREY) || (diceSum == BOXCAR))
        {
            gameStatus = Status.LOST;
        }
        //otherwise your roll is your point
        else
        {
            myPoint = diceSum;
            gameStatus = Status.CONTINUE;
            System.out.printf("Your point is %d\n",myPoint);
        }
        
        //Game loop until win or lose
        while (gameStatus == Status.CONTINUE)
        {
            //roll the dice save value
            diceSum = rollDice();
            //If its a 7 you lose
            if (diceSum == SEVEN)
            {
                gameStatus = Status.LOST;
            }
            //If its equal to your point you win
            else if (diceSum == myPoint)
            {
                gameStatus = Status.WON;
            }
        }
        
        if (gameStatus == Status.WON)
        {
            System.out.println("Player won!");
        }
        else
        {
            System.out.println("Player lost!");
        }
    }//end of main
    
    public static int rollDice()
    {
        int dice1 = randomGenerator.nextInt(6)+1;
        int dice2 = randomGenerator.nextInt(6)+1;
        int sum = dice1 + dice2;
        System.out.printf("%d+%d=%d\n",dice1,dice2,sum);
        return sum;
    }
            
}//end class
            
        