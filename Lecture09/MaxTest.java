public class MaxTest
{
    public static void main(String[] argv)
    {
        int result = maximum(3,44,5);
        System.out.printf("the max is %d", result);
        result = maximum2(3,44,5);
        System.out.printf("the max is %d", result);
        

        
    }
    
    //that given 3 values gives back the highest
    public static int maximum(int x, int y, int z)
    {
        int maxValue = x;
        if (maxValue < y)
        {
            maxValue = y;
        }
        if (maxValue < z)
        {
            maxValue = z;
        }
        return maxValue;
    }
    
    public static int maximum2(int x, int y, int z)
    {
        int maxValue = Math.max( Math.max(x,y), z);
        return maxValue;
    }
}