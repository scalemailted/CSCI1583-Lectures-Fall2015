public class Scope
{
    private static int x = 10;
    
    public static void main(String[] argv)
    {
        staticVar();
        staticVar(11.0);
        staticVar(1,2);
        
        
    }
    
    
    public static void staticVar(int x)
    {
        System.out.println("single int param");
    }
    
    public static void staticVar(double x)
    {
        System.out.println("single float param");
    }
    
        public static void staticVar(int x, int y)
    {
        System.out.println("two int param");
    }
}