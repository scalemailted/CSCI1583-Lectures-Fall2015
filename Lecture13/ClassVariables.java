public class ClassVariables
{
    private static String name="Sue";
    
    public static void main(String[] args)
    {
        
        printName();
        ClassVariables.name = "Tom";
        printName();
    }
    
    public static void printName()
    {
        System.out.println(name);
    }
}