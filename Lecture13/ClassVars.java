public class ClassVars
{
    private static String name;
    
    public static void main(String[] args)
    {
        String name = "Hello";
        ClassVars.name = name;
        printName();
    }
    
    public static void printName()
    {
        System.out.println(name);
    }
}