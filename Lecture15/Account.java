public class Account
{
	//Properties of Account
	private String name;		//name
	private double balance;		//balance

	//constructor
	public Account(String name, double balance)
	{
		this.name = name;

		if (balance > 0)
		{ 
			this.balance = balance;
		}
	}

	//getter name
	public String getName()
	{
		return this.name;
	}

	//setter name
	public void setName(String name)
	{
		this.name = name;
	}

	//getter balance
	public double getBalance()
	{
		return this.balance;
	}

	//setter balance
	public void setBalance(double balance)
	{
		this.balance = balance;
	}

	public void deposit(double dollars)
	{
		System.out.println("Hello");
		this.balance += dollars;
	}

	public void withdraw(double dollars)
	{
		if (this.balance >= dollars && dollars > 0)
		{
			this.balance -= dollars;
		}
		else
		{
			System.out.println("You don't have enough money");
		}
	}

}