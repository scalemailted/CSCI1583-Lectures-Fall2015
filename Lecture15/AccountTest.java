import java.util.Scanner;

public class AccountTest
{
	public static void main(String[] args)
	{
		Account account1 = new Account("Jane Green", 50.00);
		Account account2 = new Account("John Blue", -7.53);

		//print the initial state of account after creation
		System.out.printf("%s balance: $%.2f\n", account1.getName(), account1.getBalance() );
		System.out.printf("%s balance: $%.2f\n", account2.getName(), account2.getBalance() );

		Scanner input = new Scanner(System.in);

		System.out.printf("Enter deposit amount for %s's account: ",account1.getName() );
		double depositAmount = input.nextDouble();

		account1.deposit(depositAmount);
		System.out.printf("%s balance: $%.2f\n", account1.getName(), account1.getBalance() );
		System.out.printf("%s balance: $%.2f\n", account2.getName(), account2.getBalance() );

		System.out.printf("Enter deposit amount for %s's account: ",account2.getName() );
		depositAmount = input.nextDouble();

		account2.deposit(depositAmount);
		System.out.printf("%s balance: $%.2f\n", account1.getName(), account1.getBalance() );
		System.out.printf("%s balance: $%.2f\n", account2.getName(), account2.getBalance() );

		System.out.printf("Enter withdraw amount for %s's account: ",account2.getName() );
		depositAmount = input.nextDouble();

		account2.deposit(depositAmount);
		System.out.printf("%s balance: $%.2f\n", account1.getName(), account1.getBalance() );
		System.out.printf("%s balance: $%.2f\n", account2.getName(), account2.getBalance() )

	}
}