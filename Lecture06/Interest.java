public class Interest
{
    public static void main(String[] argv)
    {
        //amount
        double amount;
        
        //principal
        double principal = 1000000000.00;
        
        //rate
        double rate = 0.05;
        
        //print out a header
        System.out.printf("%s%20s","Year","Amount on deposit");
        
        //repetition statement that occurs 10 times
        for(int year=1;year<=10;year=year+1)
        {
            //calculate the new amount
            amount = principal * Math.pow(1.0+rate,year);
            
            //display it
            System.out.printf("%4d%,20.2f\n", year,amount);
            
        }
        
        
    }
}