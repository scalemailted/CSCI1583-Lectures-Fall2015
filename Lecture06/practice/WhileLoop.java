public class WhileLoop
{
    public static void main(String[] argv)
    {
        int counter = 1;
        while (counter <= 10)
        {
            System.out.printf("%d ",counter);
            counter++;
        }
        System.out.println();
        
        for (int i=1; i<=10; i++)
        {
            System.out.printf("%d ",i);
        }
        System.out.println();
    }
}