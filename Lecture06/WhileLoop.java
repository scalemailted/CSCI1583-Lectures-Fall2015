public class WhileLoop
{
    public static void main(String[] argv)
    {
        int counter = 1; //loop control variable
        System.out.println("do...While-Loop");
        do
        {
            System.out.printf("%d ",counter);
            counter=counter+1;
        }while (counter <=10);
        
        
    }
}