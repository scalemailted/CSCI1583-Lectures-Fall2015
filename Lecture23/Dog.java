public class Dog extends Mammal
{
    private int legs;
    
    public Dog(String name, int legs)
    {
        super(name);
        this.legs = legs;
    }
    
    public void setLegs(int legs)
    {
        this.legs = legs;
    }
    
    public int getLegs()
    {
        return this.legs;
    }
    
    public String toString()
    {
        return "dog:" + super.getName() + "leg count:" + this.legs;
    }
}