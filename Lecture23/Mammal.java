public class Mammal
{
    //properties
    boolean hasFur;
    String name;
    
    public Mammal(String name)
    {
        this.hasFur = true;
        this.name = name;
        
    }
    
    public boolean getHasFur()
    {
        return this.hasFur;
    }
    
    public void setHasFur(boolean boolval)
    {
        this.hasFur = boolval;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String toString()
    {
        return "mammal: "+ this.name;
    }
}