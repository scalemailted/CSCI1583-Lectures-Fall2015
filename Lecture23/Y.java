public class Y extends X
{
    private int y;
    
    public Y(int x, int y)
    {
        super(x);
        this.y = y;
    }
    
    public int getY()
    {
        return this.y;
    }
    
    public void setY(int y)
    {
        this.y = y;
    }
    
    public String toString()
    {
        return super.toString() + ", Y: "+ this.y;
    }
}