public class XTest
{
    public static void main(String[] args)
    {
        X x = new X(5);
        System.out.println(x);
        Y y = new Y(1,2);
        System.out.println(y);
        System.out.println(y.getX());
        X yy = new Y(1,2);
        System.out.println(yy);
        System.out.println(yy.getX());
        //this throws error though, because X has no method getY.
        //System.out.println(yy.getY());
    }
}