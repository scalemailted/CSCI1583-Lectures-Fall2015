public class MammalTest
{
    public static void main(String[] args)
    {
        Mammal monkey = new Mammal("monkey");
        System.out.println(monkey);
        Dog dog1 = new Dog("fred",300);
        System.out.println(dog1);
        System.out.println(dog1.getLegs());
        Mammal dog2 = new Dog("titan", 4);
        System.out.println(dog2);
        Dog dog3 = (Dog)(dog2);
        System.out.println(dog3.getLegs());
        
    }
}