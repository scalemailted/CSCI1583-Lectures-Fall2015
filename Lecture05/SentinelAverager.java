import java.util.Scanner;

public class SentinelAverager
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        
        //Init total to 0
        int sum = 0;
        //Init count to 0
        int count = 0;
        //init average var
        int average;
        
        
        //Prompt the user to enter the first grade
        System.out.print("Enter grade or -1 to end: ");
        //Input the first grade (possibly flag value)
        int grade = input.nextInt();
        
        //While the user has not yet entered the flag value
        while (grade != -1)
        {
            //Add this grade to the sum
            sum = sum + grade;
            //Add 1 to the count
            count = count + 1;
            //Prompt the user to enter another grade
            System.out.print("Enter grade or -1 to end: ");
            
            //Get the input for the next grade (possibly flag)
            grade = input.nextInt();
        }
            
        //If the counter is not equal to 0
        if (count > 0)
        {
            //Set the average to the sum/count
            average = sum/count;
            //Print the average
            System.out.println("The average is: "+average);
        }
        //Else
        else
        {
            //Print "No grades entered"
            System.out.println("No grades entered.");
        }
    }
}