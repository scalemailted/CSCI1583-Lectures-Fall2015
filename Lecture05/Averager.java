import java.util.Scanner;

public class Averager
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        
        //sum
        int sum = 0;
        //count
        int count = 0;
        
        //while the number of grades us less than 10
        while (count < 10)
        {
            //Prompt for a grade
            System.out.print("Enter grade: ");
            
            //Get that grade
            int grade = input.nextInt();
            
            //add that grade to our sum
            sum = grade + sum;
            
            //add one to our count
            count = count+1;
        }
            
        //average is sum / count
        int average = sum/count;
        
        //print average
        System.out.println("The sum is:" + sum);
        System.out.println("The count is:" + count);
        System.out.println("The average is:" + average);
        
        
        
        
    }
}