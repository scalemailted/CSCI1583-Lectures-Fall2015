import java.util.Scanner;
public class NestedControlStats
{
    public static void main(String[] args)
    {
        //init Scanner for user input
        Scanner input = new Scanner(System.in);
        //init passes to 0
        int passes =0;
        //init failures to 0
        int failures = 0;
        //init count to 1
        int count = 1;
        //declare result to hold user input
        int result;
        
        //while count is less than or equal to 10
        while (count <= 10)
        {
            //prompt the user to enter the next exam results
            System.out.print("Enter the test result: ");
            //input the next exam results
            result = input.nextInt();
            
            //If input is 1
            if (result == 1)
            {
                //increment passes 
                passes++;
            }
            //else
            else
            {
                //increment failures
                failures++;
            }
                
            //increment count
            count++;
        }
            
        //print the number of passes
        System.out.println("passes:" + passes);
        //print the number of failures
        System.out.println("failures:" + failures);
        
        //If passes is 8 or higher
        if (passes >= 8)
        {
            //print "Bonus"
            System.out.println("Bonus");
        }
        
        
    }
}