public class Card
{
    private String suit;
    private String rank;
    
    public Card(String suit, String rank)
    {
        this.suit = suit;
        this.rank = rank;
    }
    
    public String getSuit()
    {
        return this.suit;
    }
    
    public void setSuit(String suit)
    {
        this.suit = suit;
    }
    
    public String getRank()
    {
        return this.rank;
    }
    
    public void setRank(String rank)
    {
        this.rank = rank;
    }
    
    public String toString()
    {
        return this.rank + " of " + this.suit; 
    }
}