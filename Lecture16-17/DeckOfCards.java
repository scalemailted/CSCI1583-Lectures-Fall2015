import java.util.Random;

public class DeckOfCards
{
    //named constants
    private static final int NUMBER_OF_CARDS = 52;
    //Class Variable
    private static Random randomGenerator = new Random();
    //properties of the class
    private Card[] deck;
    private int currentCard;
    
    //
    public DeckOfCards()
    {
        String[] ranks = {"Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
        String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
        
        this.deck = new Card[NUMBER_OF_CARDS];
        
        currentCard = 0;
        
        for (int iSuit=0;iSuit < suits.length; iSuit++)
        {
            for (int iRank=0;iRank< ranks.length; iRank++)
            {
                String cardSuit = suits[iSuit];
                String cardRank = ranks[iRank];
                this.deck[currentCard] = new Card(cardSuit, cardRank);
                currentCard++;
            }//inner for loop
        }//outer for loop
        currentCard = 0;
    }//End of Constructor
    
    public void shuffle()
    {
        currentCard = 0;
        for (int first=0; first<deck.length; first++)
        {
           int second = randomGenerator.nextInt(NUMBER_OF_CARDS);
           
           Card temp = deck[first];
           deck[first] = deck[second];
           deck[second] = temp;
        }
    }
    
    public Card dealCard()
    {
        if (currentCard < deck.length)
        {
             return deck[currentCard++];
        }
        else
        {
            return null;
        }
    }
}

