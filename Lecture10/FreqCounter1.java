import java.util.Random;


public class FreqCounter
{
    
    public static Random randomizer = new Random();
    
    public static void main(String[] argv)
    {
        int[] frequency = {0,0,0,0,0,0,0};
        for (int i=0;i<6000000;i++)
        {
            int thisRoll = randomizer.nextInt(6)+1;
            frequency[thisRoll]++;
        }
        System.out.printf("%s%10s\n","Face","Frequency");
        for (int face=1;face<frequency.length;face++)
        {
            System.out.printf("%4d%10d\n",face,frequency[face]);
        }
        
    }
}