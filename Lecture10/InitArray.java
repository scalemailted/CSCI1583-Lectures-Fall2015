public class InitArray
{
    
    public static void main(String[] argv)
    {
        int[] myArray = new int[7];
        
        System.out.printf("%s%8s\n", "Index","Value");
        
        for(int counter=1;counter< myArray.length;counter++ )
        {
            System.out.printf("%5d%8d\n",counter,myArray[counter]);
        }
    }
    
}