import java.util.Arrays;

public class ArrMethods
{
    public static void main(String[] args)
    {
        double[] doubleArray = {8.4, 9.3, 0.2, 7.9, 3.4};
        Arrays.sort(doubleArray);
        System.out.printf("%ndoubleArray: ");
        for (double value: doubleArray)
        {
            System.out.printf("%.1f ", value);
        }
    }
}