public class BarChart
{
    public static void main(String[] argv)
    {
        int[] distribution = {0,5,7,100,0,0,1,2,4,2,1};
        
        System.out.println("Grade Distribution");
        
        for (int counter=0; counter<distribution.length;counter++)
        {
            if (counter == 10)
            {
                System.out.printf("%5d:", 100);
            }
            else
            {
                System.out.printf("%02d-%02d:",counter*10, counter*10+9);
            }
            for (int stars=0;stars<distribution[counter];stars++)
            {
                System.out.print("*");
            }
            System.out.print('\n');
        }
    }
}