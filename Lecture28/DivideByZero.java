import java.util.InputMismatchException;
import java.util.Scanner;

public class DivideByZero
{
    static Scanner input  = new Scanner(System.in);
    
    public static void main(String[] args)
    {
        boolean contiueLoop = true;
        do{
            try
            {
                System.out.println("Enter numbr1:");
                int num = input.nextInt();
            
                System.out.println("Enter numbr2:");
                int den = input.nextInt();
            
                int result = divide(num,den);
                System.out.printf("The result is %d\n\n", result);
                
                contiueLoop = false;
            }
            catch (InputMismatchException inputMismatchException)
            {
                System.err.printf("\nException: %s\n", inputMismatchException);
                input.nextLine();
                System.out.printf("You must enter integers\n\n");
            }
            catch (ArithmeticException arithmeticException)
            {
                System.err.printf("\nException: %s\n", arithmeticException);
                System.out.println("You cannot divide by zero, you heathen!");
            }
        }while(contiueLoop);
        
        
    }
    
    public static int divide(int num, int den) throws ArithmeticException
    {
        return num/den;
    }
    
}