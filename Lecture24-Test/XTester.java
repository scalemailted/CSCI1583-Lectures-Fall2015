public class XTester
{
    public static void main(String[] args)
    {
        X x= new X(0);
        X y = x;
        X z = new X(0);
        System.out.println("var x is: " + x);
        System.out.println("var y is: " + y);
        System.out.println("var z is: " + z);
        
        x.setX(100);
        
        System.out.println("var x is: " + x);
        System.out.println("var y is: " + y);
        System.out.println("var z is: " + z);
    }
}