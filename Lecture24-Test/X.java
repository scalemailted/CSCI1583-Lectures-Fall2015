public class X
{
    private int x;
    
    public X(int x)
    {
        this.x = x;
    }
    
    public int getX()
    {
        return this.x;
    }
    
    public void setX(int x)
    {
        this.x = x;
    }
    
    public String toString()
    {
        return "X: " + this.x;
    }
}