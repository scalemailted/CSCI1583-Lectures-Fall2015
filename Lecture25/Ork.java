public class Ork extends Monster
{
    public Ork(int hitPoints, int attackPower, int level)
    {
        super(hitPoints, attackPower, level);
    }
    
    public int attack()
    {
        return 10;
    }
}