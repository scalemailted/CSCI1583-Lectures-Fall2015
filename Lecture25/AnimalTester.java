public class AnimalTester
{
    public static void main(String[] args)
    {
        Animal a1 = new Dog("Timmie", "German Shepard", "Dog");
        printAnimal(a1);
        Animal a2 = new Frog("Kermit", 5, "Frog");
        printAnimal(a2);
      
        
    }
    
    public static void printAnimal(Animal animal)
    {
        System.out.println(animal);
        System.out.printf("The number of animals: %d\n",animal.getAnimalCount());
        System.out.printf("movement: %d\n", animal.move());
        System.out.printf("Dogs: %d\n", Dog.getDogCount());
        System.out.printf("Frogs: %d\n", Frog.getFrogCount());
    }
}