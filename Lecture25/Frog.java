public class Frog extends Animal
{
    private static int frogCount=0;
    private int jumpHeight;
    private String froggieName;
    
    public Frog(String froggieName, int jumpHeight, String name)
    {
        super(name);
        this.jumpHeight = jumpHeight;
        this.froggieName = froggieName;
        Frog.frogCount++;
    }
    
    public static int getFrogCount()
    {
        return Frog.frogCount;
    }
    
    @Override
    public int move()
    {
        return 2 * jumpHeight;
    }
    
    @Override
    public String toString()
    {
        return String.format("Animal type: %s, jump: %d, Name: %s",super.getName(), this.jumpHeight, this.froggieName);
    }
    
}