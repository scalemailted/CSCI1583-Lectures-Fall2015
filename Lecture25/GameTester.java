public class GameTester
{
    public static void main(String[] args)
    {
        Monster g1 = new Goblin(100,10,1);
        Monster o1 = new Ork(200,10,1);
        //Monster m1 = new Monster(200,10,1);
        System.out.printf("the goblin attack is %d\n",g1.attack());
        System.out.printf("the ork attack is %d\n",o1.attack());
        System.out.printf("the monster count is %d\n",Monster.getMonsterCount());
        printMonster(g1);
        
        
    }
    
    public static void printMonster(Monster monster)
    {
        System.out.printf("the monster attack is %d\n",monster.attack());
    }
}