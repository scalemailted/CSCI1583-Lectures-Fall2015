public class Dog extends Animal
{
    private static int dogCount=0;
    private String breed;
    private String doggieName;
    
    public Dog(String doggieName, String breed, String name)
    {
        super(name);
        this.breed = breed;
        this.doggieName = doggieName;
        Dog.dogCount++;
    }
    
    public static int getDogCount()
    {
        return Dog.dogCount;
    }
    
    @Override
    public int move()
    {
        return 5;
    }
    
    @Override
    public String toString()
    {
        return String.format("Animal type: %s, Breed: %s, Name: %s",super.getName(), this.breed, this.doggieName);
    }
    
}