public abstract class Animal
{
    private static int animalCount=0;
    private String name;
    
    public Animal(String name)
    {
        this.name = name;
        Animal.animalCount++;
    }
    
    public abstract int move();
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public static int getAnimalCount()
    {
        return Animal.animalCount;
    }
    
    @Override
    public String toString()
    {
        return String.format("%s",this.name);
    }
}