public class Goblin extends Monster
{
    public Goblin(int hitPoints, int attackPower, int level)
    {
        super(hitPoints, attackPower, level);
    }
    
    public int attack()
    {
        return 3;
    }
}