public abstract class Monster
{
    private static int count;
    private int hitPoints;
    private int attackPower;
    private int level;
    
    public Monster(int hitPoints, int attackPower, int level)
    {
        if (hitPoints <= 0)
        {
            throw new IllegalArgumentException("Monster health must be greater than 0");
        }
        if (attackPower <= 0 )
        {
            throw new IllegalArgumentException("Monster attack power must be greater than 0");
        }
        if (level <= 0)
        {
            throw new IllegalArgumentException("Monster level must be greater than 0");
        }
        this.hitPoints = hitPoints;
        this.attackPower = attackPower;
        this.level = level;
        count++;
    }
    
    public void takeDamage(int damage)
    {
        this.hitPoints -= damage;
    }
    
    public abstract int attack();
    
    public static int getMonsterCount()
    {
        return Monster.count;
    }

}