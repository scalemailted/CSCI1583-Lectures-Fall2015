//Application to test program processing invoices and employees polymorphically
public class InterfaceTester
{
    public static void main(String[] args)
    {
        //create a four-element Payable array
        Payable[] payableObjects = new Payable[4];
        
        //populate array with objects that implement payable
        payableObjects[0] = new Invoice("01234", "seat", 2, 375.00);
        payableObjects[1] = new Invoice("56789", "tire", 4, 79.95);
        payableObjects[2] = new SalariedEmployee("John", "Smith", "111-11-1111", 800.00);
        payableObjects[3] = new SalariedEmployee("Lisa", "Barnes", "888-88-8888", 1200.00);
        
        /*Print out the contents of the payable array*/
        //Header
        System.out.println("Invoices & Employees processed polymorphically");
        //generically process each element in array payableObjects
        for (Payable currentPayable : payableObjects)
        {
            System.out.printf("\n%s \npayment due: $%,.2f\n", currentPayable, currentPayable.getPaymentAmount());
        }//end for-loop
    }//end main
}//end class