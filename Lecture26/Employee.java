public abstract class Employee implements Payable
{
    //class properties
    private String firstName;
    private String lastName;
    private String socialSecurity;
    
    //constructor
    public Employee(String firstName, String lastName, String socialSecurity)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurity = socialSecurity;
    }
    
    //Getter: get first name
    public String getFirstName()
    {
        return this.firstName;
    }
    
    //Getter: get last name
    public String getLastName()
    {
        return this.lastName;
    }
    
    //Getter: get social security
    public String getSocialSecurity()
    {
        return this.socialSecurity;
    }
    
    //toString: Strignify this object
    @Override
    public String toString()
    {
        return String.format("%s %s\nsocial security: %s",getFirstName(),getLastName(),getSocialSecurity());
    }
    
    //Note: we do not implement Payable method getPaymentAmount here
    //so this class must be delared abstract to avoid a compilation error
}