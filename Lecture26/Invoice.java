public class Invoice implements Payable
{
    //Class Properties
    private String partNumber;
    private String partDescription;
    private int quantity;
    private double pricePerItem;
    
    //Constructor
    public Invoice(String partNumber, String partDescription, int quantity, double pricePerItem)
    {
        this.partNumber = partNumber;
        this.partDescription = partDescription;
        this.quantity = quantity;
        this.pricePerItem = pricePerItem;
    }
    
    //Getter: Get part number
    public String getPartNumber()
    {
        return this.partNumber;
    }
    
    //Getter: Get description
    public String getPartDescription()
    {
        return this.partDescription;
    }
    
    //Setter: Set quantity
    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }
    
    //Getter: Get quantity
    public int getQuantity()
    {
        return this.quantity;
    }
    
    //Setter: Set price per item
    public void setPricePerItem(double pricePerItem)
    {
        this.pricePerItem = pricePerItem;
    }
    
    //Getter: Get price per item
    public double getPricePerItem()
    {
        return this.pricePerItem;
    }
    
    //toString: Stringify this object
    @Override
    public String toString()
    {
        return String.format("%s: \n%s: quantity (%d), price per item: $%,.2f ",getPartNumber(), getPartDescription(), getQuantity(), getPricePerItem());
    }
    
    //getPaymentAmount
    @Override
    public double getPaymentAmount()
    {
        return getQuantity() * getPricePerItem();
    }
}