public class SalariedEmployee extends Employee
{
    //Class Properties
    private double salary;
    
    //Constructor
    public SalariedEmployee(String firstName, String lastName, String socialSecurity, double salary)
    {
        super(firstName, lastName, socialSecurity);
        this.salary = salary;
    }
    //Setter: set salary
    public void setSalary(double salary)
    {
        this.salary = salary;
    }
    
    //Getter:  get salary
    public double getSalary()
    {
        return this.salary;
    }
    
    //Override the interface: getPaymentAmount 
    @Override
    public double getPaymentAmount()
    {
        return getSalary();
    }
    
    //Override the toString
    @Override
    public String toString()
    {
        return String.format("%s, salary: $%,.2f",super.toString(), getSalary());
    }
}