public class Bonus {
    
    public static void main(String[] args) {
        Integer[] intArray = {1, 2, 3, 4, 5, 6};
        
        for(Integer i : intArray) {
            System.out.print(i+" ");
        }
        
        reverseArray((Object[]) intArray);
        
        System.out.println();
        
        for(Integer i : intArray) {
            System.out.print(i+" ");
        }
    }
    
    public static void reverseArray(Object[] inputArray) {
        int arrayLength = inputArray.length;
        for(int i = arrayLength-1; i>(arrayLength/2)-1; i--) {
            Object oppositeIndex = inputArray[arrayLength-i-1];
            inputArray[arrayLength-i-1] = inputArray[i];
            inputArray[i] = oppositeIndex;
        }
    }
    
}