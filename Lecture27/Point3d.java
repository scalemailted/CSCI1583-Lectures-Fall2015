public class Point3d
{
    //Class properties
    double x;
    double y;
    double z;
    
    //Constructor
    public Point3d(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    //Getters
    public double getX()
    {
        return this.x;
    }
    
    public double getY()
    {
        return this.y;
    }
    
    public double getZ()
    {
        return this.z;
    }
    
    //Move To
    public void moveTo(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    //Move By
    public void moveBy(double dx, double dy, double dz)
    {
        this.x += dx;
        this.y += dy;
        this.z += dz;
    }
    
    //Distance 
    public double distance(Point3d otherPoint)
    {
        double x1 = this.x;
        double y1 = this.y;
        double z1 = this.z;
        double x2 = otherPoint.getX();
        double y2 = otherPoint.getY();
        double z2 = otherPoint.getZ();
        
        double dx = (x1 - x2) * (x1 - x2);
        double dy = (y1 - y2) * (y1 - y2);
        double dz = (z1 - z2) * (z1 - z2);
        
        double d = Math.sqrt(dx + dy + dz)
        return d;
    }
}